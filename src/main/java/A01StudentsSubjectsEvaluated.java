import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.*;

public class A01StudentsSubjectsEvaluated {

    public static void main(String[] args) {

        SparkConf conf= new SparkConf().setAppName("School-Evaluation").setMaster("local[1]");
        SparkSession sparkSession = SparkSession.builder().config(conf).getOrCreate();

        // extract DataSet Evaluations
        Dataset<Row> evaluationsDataset = sparkSession.read().json("input/evaluations.json");

        // extract DataSet students
        Dataset<Row> studentsDataset = sparkSession.read().json("input/students.json");

        // Left join des dataframes students et evaluation
        Dataset<Row> studentEval = studentsDataset.join(evaluationsDataset, studentsDataset.col("id_student")
                .equalTo(evaluationsDataset.col("student")), "left");

        //For each student, find every subjects on which he has been evaluated.
        studentEval = studentEval.select(concat_ws(" ", studentEval.col("name"),
                studentEval.col("surname")).alias("name"), studentEval.col("subject"));
        studentEval = studentEval.distinct();
        studentEval = studentEval.groupBy(studentEval.col("name"))
                .agg(collect_list(studentEval.col("subject").alias("subjects")));

        studentEval.write().mode("overwrite").json("output/A01-StudentsSubjectsEvaluated");
        studentEval.show(false);
    }
}
