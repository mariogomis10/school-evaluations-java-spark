import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import static org.apache.spark.sql.functions.*;

public class Main {

    public static void main(String[] args) {

        SparkConf conf= new SparkConf().setAppName("School-Evaluation").setMaster("local[1]");
        SparkSession sparkSession = SparkSession.builder().config(conf).getOrCreate();

        // extract DataSet Evaluations
        String evaluationsFilePath = Main.class.getResource("evaluations.json").getPath();
        Dataset<Row> evaluationsDataset = sparkSession.read().json(evaluationsFilePath);
        evaluationsDataset.show();

        // extract DataSet teachers
        String teachersFilePath = Main.class.getResource("teachers.json").getPath();
        Dataset<Row> teacherDataset = sparkSession.read().json(teachersFilePath);
        teacherDataset.show();

        // extract DataSet students
        String studentsFilePath = Main.class.getResource("students.json").getPath();
        Dataset<Row> studentsDataset = sparkSession.read().json(studentsFilePath);
        studentsDataset.show();

    }
}
