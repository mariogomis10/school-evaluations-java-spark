import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.*;

public class A03ClassesAverageOnSubjects {

    public static void main(String[] args) {

        SparkConf conf= new SparkConf().setAppName("School-Evaluation").setMaster("local[1]");
        SparkSession sparkSession = SparkSession.builder().config(conf).getOrCreate();

        // extract DataSet Evaluations
        Dataset<Row> evaluationsDataset = sparkSession.read().json("input/evaluations.json");

        // extract DataSet students
        Dataset<Row> studentsDataset = sparkSession.read().json("input/students.json");

        // Left join des dataframes students et evaluation
        Dataset<Row> studentEval = studentsDataset.join(evaluationsDataset, studentsDataset.col("id_student")
                .equalTo(evaluationsDataset.col("student")), "left");

        //Calculate the average of each class in each subject
        studentEval = studentEval.select(studentEval.col("class"), studentEval.col("subject"), studentEval.col("score"));
        studentEval = studentEval.groupBy(studentEval.col("class"), studentEval.col("subject"))
                .agg(avg(studentEval.col("score")).alias("average_score"));
        studentEval = studentEval.select(studentEval.col("class"), studentEval.col("subject"), round(studentEval.col("average_score"), 2).alias("average_score"));
        studentEval.write().mode("overwrite").json("output/A03ClassesAverageOnSubjects");
        studentEval.show(false);

    }
}
